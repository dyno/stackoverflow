#!/usr/bin/osascript

## AppleScript Editor -> File -> Open Directory ... -> Xcode 4.2.1 ##

# http://vgable.com/blog/2009/04/24/how-to-check-if-an-application-is-running-with-applescript/
on ApplicationIsRunning(appName)
	tell application "System Events" to set appNameIsRunning to exists (processes where name is appName)
	return appNameIsRunning
end ApplicationIsRunning

if not ApplicationIsRunning("Xcode") then
	log ("Launching Xcode ...")
	launch application id "com.apple.dt.Xcode"
	delay 5
else
	log ("Xcode is already running ...")
end if

# set project_path to "/PATH/TO/YOUR_PROJECT.xcodeproj"
tell application "Finder"
	set _home to name of home
end tell
set project_path to "/Users/" & _home & "/bitbucket/keysing-iphone/KeySing.xcodeproj"
set _project_name to (do shell script "echo $(basename " & project_path & ") | sed -e 's/.xcodeproj//'")
log ("_project_name set to " & _project_name)
# set _target_name to "YOUR_TARGET"
set _target_name to "KeySing"
log ("_target_name set to " & _target_name)

tell application id "com.apple.dt.Xcode"
	log ("Open an XCode project at " & project_path)
	set _project_file to (POSIX file project_path as alias)
	open _project_file
	
	set _workspace to active workspace document
	set _project to first item of (projects of _workspace whose name = _project_name)
	set _target to first item of (targets of _project whose name = _target_name)
	
	# as this won't work, cannot event resort to system event
	# duplicate _target with properties {name:_target_name & "_clone"}
	# make new build configuration with data build configurations of _target with properties {name:_target_name & "_clone"}
	# activate
	
	log ("Edit the Build Settings of the new target")
	set _build_configuration to first item of (build configurations of _target whose name = "Debug")
	set value of build setting "PRODUCT_NAME" of _build_configuration to "KeySING"
	
	# http://lists.apple.com/archives/xcode-users//2008/Feb/msg00497.html
	log ("Add a group to the Source and Resources section, then rename them")
	set _new_group_name to "groupX"
	tell root group of _project
		set _groups to (get groups whose name = _new_group_name)
		if (length of _groups) = 0 then
			set _new_group to make new group with properties {name:_new_group_name, path:_new_group_name, path type:group relative}
		else
			set _new_group to first item of _groups
		end if
		
		log ("Add source files to the groups, and add the file to the new Target")
		tell _new_group
			set _new_file_name to "whatever.h"
			set _new_files to get (file references whose name = _new_file_name)
			if (length of _new_files) = 0 then
				# Xcode crashes
				# make new file reference with properties {name:_new_file_name, file encoding:utf8, path type:group relative, path:_new_file_name}
			end if
		end tell
	end tell
	
	log ("Close XCode")
	quit
	
end tell


